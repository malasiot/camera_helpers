#ifndef _CAMERA_HELPERS_UTIL_H_
#define _CAMERA_HELPERS_UTIL_H_

#include <cv.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>

#include <image_geometry/pinhole_camera_model.h>

namespace camera_helpers {

// convert opencv image to ROS message
sensor_msgs::ImagePtr cvImageToMsg(const cv::Mat  &image) ;

// convert ROS message to opencv image
cv::Mat msgToCvImage(const sensor_msgs::ImageConstPtr &msg) ;
cv::Mat msgToCvImage(const sensor_msgs::Image &msg) ;

sensor_msgs::PointCloud2Ptr depthToPointCloudMsg(const cv::Mat &depth,  const image_geometry::PinholeCameraModel& model, const std::string &frame_id) ;
sensor_msgs::PointCloud2Ptr rgbdToPointCloudMsg(const cv::Mat &rgb, const cv::Mat &depth,  const image_geometry::PinholeCameraModel& model,  const std::string &frame_id) ;

}

#endif


