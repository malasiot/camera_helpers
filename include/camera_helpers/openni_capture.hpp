#ifndef __OPENNI_CAPTURE_H__
#define __OPENNI_CAPTURE_H__

#include <ros/ros.h>
#include <string>
#include <opencv2/opencv.hpp>

#include <image_geometry/pinhole_camera_model.h>

namespace camera_helpers {

// Helper classes for asynchronous capture of OpenNI camera frame
// Consuming the openni.launch associated topics

class OpenNICaptureImpl ;

class OpenNICaptureBase {
public:

    // Create a grabber for the specified camera prefix e.g. xtion2
    virtual ~OpenNICaptureBase() ;

    // Connect to the camera waiting for data to become available.
    bool connect(ros::Duration timeout = ros::Duration(-1)) ;

    // non blocking version that signals a callback when data is ready
    void connect(boost::function<void ()> cb, ros::Duration timeout = ros::Duration(-1)) ;

    // disconnect from the camera
    void disconnect() ;

    bool isConnected() const ;

protected:

    boost::shared_ptr<OpenNICaptureImpl> impl_ ;

};

// capture an RGB and Depth image pair (uses registered frame)

class OpenNICaptureRGBD: public OpenNICaptureBase {

public:

    // Create a grabber for the specified camera topic prefix e.g. xtion2
    OpenNICaptureRGBD(const std::string &prefix) ;

    // Grab images. Also get the timestamp the depth image and the pinhole camera model of the (registered) depth frame

    bool grab(cv::Mat &clr, cv::Mat &depth, ros::Time &ts, image_geometry::PinholeCameraModel &cm, bool scale_depth = true) ;

} ;


} // namespace camera_helpers


#endif
