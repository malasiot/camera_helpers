#ifndef __GPHOTO2_CAPTURE_HPP__
#define __GPHOTO2_CAPTURE_HPP__

#include <string>
#include <opencv2/opencv.hpp>

namespace camera_helpers {

// simple client that calls the gphoto2_service

namespace gphoto2 {

    // Capture single image. srv_prefix is the prefix of the associated service

    bool capture(cv::Mat &im, const std::string srv_prefix = std::string()) ;

    // set configuration of the camera

    bool setConfig(const std::string &name, const std::string &val, const std::string srv_prefix = std::string()) ;

    // get configuration from camera

    bool getConfig(const std::string &name, std::string &val, const std::string srv_prefix = std::string()) ;
}
}

#endif
