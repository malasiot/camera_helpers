#include <ros/ros.h>
#include <camera_helpers/gphoto2_capture.hpp>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>
#include <time.h>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "get_image");
    ros::NodeHandle nh;
    image_transport::ImageTransport it(nh);
    image_transport::Publisher pub = it.advertise("camera/Image", 1);

    cv::Mat rgb;

    if ( camera_helpers::gphoto2::capture(rgb, "nikon_overhead") ) {
        cv::imwrite("/tmp/cap.png", rgb) ;
    }
    else {
      std::cerr << "Could not query photo/capture service" << std::endl ;
    }

    time_t rawtime;
    struct tm * timeinfo;
    char buffer[80];
    time (&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(buffer,sizeof(buffer),"/home/mitre/Pictures/test_folder/%d_%m_%I_%M.png",timeinfo);
    imwrite(buffer, rgb);

    cv_bridge::CvImage cv_img(std_msgs::Header(), "bgr8", rgb);

    sensor_msgs::ImagePtr msg = cv_img.toImageMsg();

    ros::Rate loop_rate(0.1);

    while(ros::ok())
    {
        pub.publish(msg);
        ros::spinOnce();
        loop_rate.sleep();
    }
}
