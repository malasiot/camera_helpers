#include <iostream>
#include <ros/ros.h>

#include <camera_helpers/gphoto2_capture.hpp>

using namespace std;

void test_capture() {

    cv::Mat im ;
    if ( camera_helpers::gphoto2::capture(im, "nikon_overhead") ) {
        cv::imwrite("/tmp/cap.png", im) ;
    }
    else {
      cerr << "Could not query photo/capture service" << endl ;
    }
}

void test_get_config() {
    string val ;
    if ( camera_helpers::gphoto2::getConfig("main/capturesettings/f-number", val, "nikon_overhead") ) {
        cout << val << endl ;
    }
    else {
      cerr << "Could not query gphoto get config service" << endl ;
    }

}

void test_capture_index(uint &i) {

    cv::Mat im ;
    if ( camera_helpers::gphoto2::capture(im, "nikon_overhead") ) {
        cv::imwrite("/tmp/im" + to_string(i) + ".png", im) ;
        i++;
    }
    else {
      cerr << "Could not query photo/capture service" << endl ;
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "openni_capture_example");
    ros::NodeHandle nh;
//    test_capture() ;
//    test_get_config() ;

    //capturing photo every five seconds
    ros::Rate loop_rate(0.2);

    uint i = 0;

    while(ros::ok())
    {
        test_capture_index(i);
        ros::spinOnce();
        loop_rate.sleep();

        if (i>=30) break;
    }

  return 0;
}
