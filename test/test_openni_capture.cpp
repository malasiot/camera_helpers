#include <Eigen/Core>

#include <cvx/util/imgproc/rgbd.hpp>
#include <camera_helpers/openni_capture.hpp>
#include <camera_helpers/util.hpp>

#include <tf/transform_broadcaster.h>

#include <string>

using namespace Eigen ;
using namespace std ;
using namespace camera_helpers ;

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "openni_capture_example");
    ros::NodeHandle nh ;
    ros::Publisher pub = nh.advertise<sensor_msgs::PointCloud2>("grabcloud", 1);

    tf::Transform tr ;
    tr.setOrigin(tf::Vector3(0, 0, 0));
    tr.setRotation(tf::Quaternion(0, 0, 0, 1));


    tf::TransformBroadcaster brdc ;


    if (argc < 1)
        std::cout << "Please specify camera" << std::endl;

   image_geometry::PinholeCameraModel camera ;
   std::string cameraName = argv[1] ;

   OpenNICaptureRGBD grabber(cameraName) ;

    if ( grabber.connect( ros::Duration(2) ) )
    {
        std::cout << "start grabbing RGBD" << std::endl ;

        cv::Mat clr, depth ;
        ros::Time ts ;

        for(int i=0 ; i<40 ; i++ )
        {
            brdc.sendTransform(tf::StampedTransform(tr, ros::Time(0), "map", "xtion3_rgb_optical_frame")) ;

            if ( grabber.grab(clr, depth, ts, camera) )
            {
                cv::imwrite(str(boost::format("/tmp/rgb_%03d.png") % i), clr) ;
                cv::imwrite(str(boost::format("/tmp/depth_%03d.png") % i), /*cvx::util::depthViz(depth)*/depth) ;
                std::cout << "grab" << std::endl ;

                pub.publish(rgbdToPointCloudMsg(clr, depth, camera, "xtion3_rgb_optical_frame")) ;
            }
            else { std::cout << "error" << std::endl ; }

            ros::Duration(1).sleep() ;
        }

        std::cout << "finished RGBD" << std::endl ;

        grabber.disconnect();

    }
    else {
        ROS_INFO("cannot connect to camera") ;

    }


    return 0 ;

}

