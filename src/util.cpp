#include <camera_helpers/util.hpp>

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/point_cloud2_iterator.h>
#include <image_geometry/pinhole_camera_model.h>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <limits>

namespace enc = sensor_msgs::image_encodings;

namespace camera_helpers {

sensor_msgs::ImagePtr cvImageToMsg(const cv::Mat  &image)
{
    assert( image.data ) ;

    cv_bridge::CvImage image_ ;
    image_.image = image ;
    image_.header.stamp = ros::Time::now() ;

    sensor_msgs::ImagePtr msg ;

    if ( image.type() == CV_16UC1 )
        image_.encoding = "mono16" ;
    else if ( image.type() == CV_8UC3 )
        image_.encoding = "bgr8";
    else if ( image.type() == CV_8UC1 )
        image_.encoding = "mono8";
    else if ( image.type() == CV_32FC1 )
        image_.encoding = enc::TYPE_32FC1;

    msg = image_.toImageMsg() ;

    return msg ;
}

cv::Mat msgToCvImage(const sensor_msgs::ImageConstPtr &msg)
{
    cv_bridge::CvImagePtr image_ ;

    if ( msg->encoding == "bgr8" || msg->encoding == "rgb8")
        image_ = cv_bridge::toCvCopy(msg, enc::BGR8);
    else if ( msg->encoding == enc::TYPE_32FC1 )
        image_ = cv_bridge::toCvCopy(msg, enc::TYPE_32FC1);
    else if ( msg->encoding == enc::TYPE_16UC1 )
        image_ = cv_bridge::toCvCopy(msg, "mono16");
    else if ( msg->encoding == "mono8" )
        image_ = cv_bridge::toCvCopy(msg, "mono8");
    else if ( msg->encoding == enc::TYPE_8UC1 )
        image_ = cv_bridge::toCvCopy(msg, enc::TYPE_8UC1);
    return image_->image ;
}

cv::Mat msgToCvImage(const sensor_msgs::Image &msg)
{
    boost::shared_ptr<sensor_msgs::Image> msgPtr(boost::make_shared<sensor_msgs::Image>(msg));

    cv_bridge::CvImagePtr image_ ;

    if ( msgPtr->encoding == "bgr8" || msgPtr->encoding == "rgb8")
        image_ = cv_bridge::toCvCopy(msgPtr, enc::BGR8);
    else if ( msgPtr->encoding == enc::TYPE_32FC1 )
        image_ = cv_bridge::toCvCopy(msgPtr, enc::TYPE_32FC1);
    else if ( msgPtr->encoding == enc::TYPE_16UC1 )
        image_ = cv_bridge::toCvCopy(msgPtr, "mono16");
    else if ( msgPtr->encoding == "mono8" )
        image_ = cv_bridge::toCvCopy(msgPtr, "mono8");
    else if ( msgPtr->encoding == enc::TYPE_8UC1 )
        image_ = cv_bridge::toCvCopy(msgPtr, enc::TYPE_8UC1);
    return image_->image ;
}

typedef sensor_msgs::PointCloud2 PointCloud;

PointCloud::Ptr depthToPointCloudMsg(const cv::Mat &depth, const image_geometry::PinholeCameraModel& model, const std::string &frame_id) {

    assert( depth.type() == CV_16UC1 || depth.type() == CV_32FC1 ) ;

    // Use correct principal point from calibration
    float center_x = model.cx();
    float center_y = model.cy();

    // Combine unit conversion (if necessary) with scaling by focal length for computing (X,Y)

    float bad_point = std::numeric_limits<float>::quiet_NaN();
    float constant_x = 1.0 / model.fx();
    float constant_y = 1.0 / model.fy();

    PointCloud::Ptr cloud_msg(new PointCloud) ;

    cloud_msg->header.frame_id = frame_id ;
    cloud_msg->header.stamp = ros::Time(0) ;

    cloud_msg->height = depth.rows;
    cloud_msg->width  = depth.cols;
    cloud_msg->is_dense = false;
    cloud_msg->is_bigendian = false;

     sensor_msgs::PointCloud2Modifier pcd_modifier(*cloud_msg);
    pcd_modifier.setPointCloud2FieldsByString(1, "xyz");

    sensor_msgs::PointCloud2Iterator<float> iter_x(*cloud_msg, "x");
    sensor_msgs::PointCloud2Iterator<float> iter_y(*cloud_msg, "y");
    sensor_msgs::PointCloud2Iterator<float> iter_z(*cloud_msg, "z");

    if ( depth.type() == CV_16UC1 ) {
        const ushort *depth_row = reinterpret_cast<const ushort *>(depth.data);
        int row_step = depth.step / sizeof(ushort);

        for (int v = 0; v < (int)depth.rows; ++v, depth_row += row_step)
        {
            for (int u = 0; u < (int)depth.cols; ++u, ++iter_x, ++iter_y, ++iter_z)
            {
                ushort depth = depth_row[u];

                // Missing points denoted by NaNs
                if ( depth == 0 ) {
                    *iter_x = *iter_y = *iter_z = bad_point;
                } else {
                    float scaled_depth = depth * 0.001 ;
                    *iter_x = (u - center_x) * scaled_depth * constant_x ;
                    *iter_y = (v - center_y) * scaled_depth * constant_y ;
                    *iter_z = scaled_depth ;
                }
            }
        }
    }
    else {
        const float *depth_row = reinterpret_cast<const float *>(depth.data);
        int row_step = depth.step / sizeof(float);

        for (int v = 0; v < (int)depth.rows; ++v, depth_row += row_step)
        {
            for (int u = 0; u < (int)depth.cols; ++u, ++iter_x, ++iter_y, ++iter_z)
            {
                float depth = depth_row[u];

                // Missing points denoted by NaNs
                if ( !std::isfinite(depth) ) {
                    *iter_x = *iter_y = *iter_z = bad_point;
                } else {
                    *iter_x = (u - center_x) * depth * constant_x;
                    *iter_y = (v - center_y) * depth * constant_y;
                    *iter_z = depth;
                }
            }
        }

    }

    return cloud_msg ;

}

sensor_msgs::PointCloud2Ptr rgbdToPointCloudMsg(const cv::Mat &clr, const cv::Mat &depth, const image_geometry::PinholeCameraModel &model, const std::string &frame_id)
{
    assert( depth.type() == CV_16UC1 || depth.type() == CV_32FC1 ) ;

    // Use correct principal point from calibration
    float center_x = model.cx();
    float center_y = model.cy();

    float constant_x = 1.0 / model.fx();
    float constant_y = 1.0 / model.fy();

    float bad_point = std::numeric_limits<float>::quiet_NaN ();

    const uint8_t* rgb = &clr.data[0];
    int rgb_skip = clr.step - clr.cols * 3;

    PointCloud::Ptr cloud_msg(new PointCloud) ;

    cloud_msg->header.frame_id = frame_id ;
    cloud_msg->header.stamp = ros::Time(0) ;

    cloud_msg->height = depth.rows;
    cloud_msg->width  = depth.cols;
    cloud_msg->is_dense = false;
    cloud_msg->is_bigendian = false;

    sensor_msgs::PointCloud2Modifier pcd_modifier(*cloud_msg);
    pcd_modifier.setPointCloud2FieldsByString(2, "xyz", "rgb");

    sensor_msgs::PointCloud2Iterator<float> iter_x(*cloud_msg, "x");
    sensor_msgs::PointCloud2Iterator<float> iter_y(*cloud_msg, "y");
    sensor_msgs::PointCloud2Iterator<float> iter_z(*cloud_msg, "z");
    sensor_msgs::PointCloud2Iterator<uint8_t> iter_r(*cloud_msg, "r");
    sensor_msgs::PointCloud2Iterator<uint8_t> iter_g(*cloud_msg, "g");
    sensor_msgs::PointCloud2Iterator<uint8_t> iter_b(*cloud_msg, "b");
    sensor_msgs::PointCloud2Iterator<uint8_t> iter_a(*cloud_msg, "a");

    if ( depth.type() == CV_16UC1 ) {
        const ushort *depth_row = reinterpret_cast<const ushort *>(depth.data);
        int row_step = depth.step / sizeof(ushort);

        for (int v = 0; v < (int)depth.rows; ++v, depth_row += row_step, rgb += rgb_skip)
        {
            for (int u = 0; u < (int)depth.cols; ++u, ++iter_x, ++iter_y, ++iter_z, rgb += 3, ++iter_a, ++iter_r, ++iter_g, ++iter_b)
            {
                ushort depth = depth_row[u];

                // Missing points denoted by NaNs
                if ( depth == 0 ) {
                    *iter_x = *iter_y = *iter_z = bad_point;
                } else {
                    // Fill in XYZ
                    float scaled_depth = depth * 0.001 ;
                    *iter_x = (u - center_x) * scaled_depth * constant_x ;
                    *iter_y = (v - center_y) * scaled_depth * constant_y ;
                    *iter_z = scaled_depth ;
                }

                // Fill in color
                *iter_a = 255;
                *iter_r = rgb[2];
                *iter_g = rgb[1];
                *iter_b = rgb[0];
            }
        }
    }
    else {
        const float *depth_row = reinterpret_cast<const float *>(depth.data);
        int row_step = depth.step / sizeof(float);

        for (int v = 0; v < (int)depth.rows; ++v, depth_row += row_step, rgb += rgb_skip)
        {
            for (int u = 0; u < (int)depth.cols; ++u, ++iter_x, ++iter_y, ++iter_z, rgb += 3, ++iter_a, ++iter_r, ++iter_g, ++iter_b)
            {
                float depth = depth_row[u];

                // Missing points denoted by NaNs
                if ( !std::isfinite(depth) ) {
                    *iter_x = *iter_y = *iter_z = bad_point;
                } else {
                    // Fill in XYZ
                    *iter_x = (u - center_x) * depth * constant_x;
                    *iter_y = (v - center_y) * depth * constant_y;
                    *iter_z = depth;
                }

                // Fill in color
                *iter_a = 255;
                *iter_r = rgb[2];
                *iter_g = rgb[1];
                *iter_b = rgb[0];
            }

        }
    }

    return cloud_msg ;
}


}
