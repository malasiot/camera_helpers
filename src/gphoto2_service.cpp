#include <ros/ros.h>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include <cvx/util/misc/arg_parser.hpp>

#include <camera_helpers/gphoto2_capture.h>
#include <camera_helpers/gphoto2_get_config.h>
#include <camera_helpers/gphoto2_set_config.h>

#include <camera_helpers/util.hpp>

#include "gphoto2/camera.hpp"

using namespace std ;
using namespace sensor_msgs;
using namespace message_filters;
using namespace cvx::util ;

class GPhoto2Service {

public:

    GPhoto2Service() {}

    bool start(const string &camera_id, ros::NodeHandle &nh) {

        if ( !camera_list_.autoDetect() )  return false ;

        if ( camera_id.substr(0, 4) == "usb:" ) {
            if ( !camera_.open(camera_list_, camera_id ) ) return false ;
        }
        else {
            try {
                int idx = stoi(camera_id) ;
                if ( !camera_.open(camera_list_, idx) ) return false ;
            }
            catch ( ... ) {
                return false ;
            }
        }

        set_config_srv_ = nh.advertiseService("set_config", &GPhoto2Service::setConfig, this);
        get_config_srv_ = nh.advertiseService("get_config", &GPhoto2Service::getConfig, this);
        capture_srv_ = nh.advertiseService("capture", &GPhoto2Service::capture, this);

        ROS_INFO_STREAM("Starting gphoto2_service for camera: " << camera_.name() << " at " << camera_.port()) ;

        return true ;

    }

    bool setConfig( camera_helpers::gphoto2_set_config::Request& req, camera_helpers::gphoto2_set_config::Response& resp )
    {
        std::lock_guard<std::mutex> lock(cam_mutex_) ;

        return camera_.setConfig(req.param, req.value) ;

        return true ;
    }

    bool getConfig( camera_helpers::gphoto2_get_config::Request& req, camera_helpers::gphoto2_get_config::Response& resp )
    {
        std::lock_guard<std::mutex> lock(cam_mutex_) ;

        return camera_.getConfig(req.param, resp.value) ;
    }

    bool capture( camera_helpers::gphoto2_capture::Request& req, camera_helpers::gphoto2_capture::Response& resp )
    {
        std::lock_guard<std::mutex> lock(cam_mutex_) ;

        cv::Mat res ;
        if ( camera_.capture(res) ) {
            resp.image = *camera_helpers::cvImageToMsg(res) ;
            return true ;
        }
        else return false ;
    }

    gphoto2::CameraList camera_list_ ;
    gphoto2::Camera camera_ ;

    ros::ServiceServer set_config_srv_;
    ros::ServiceServer get_config_srv_;
    ros::ServiceServer capture_srv_;

    std::mutex cam_mutex_ ;

};


int main(int argc, char *argv[])
{
    ros::init(argc, argv, "gphoto2_service"  );

    ArgumentParser args ;

    bool print_help = false ;
    string camera_id, ns ;

    args.addOption("-h|--help", print_help, true).setMaxArgs(0).setDescription("print this help message") ;
    args.addOption("--ns", ns).setDescription("namespace prefix").setName("<namespace>") ;
    args.addOption("--camera", camera_id).setDescription("camera idx or port (e.g. \"usb:007,010\")").setName("<camera id>") ;

    if ( !args.parse(argc, (const char **)argv) || print_help ) {
        cout << "Usage: gphoto2_service [options]" << endl ;
        cout << "Options:" << endl ;
        args.printOptions(std::cout) ;
        exit(1) ;
    }

    ros::NodeHandle nh(ns.empty() ? "~": ns), nh_g ;

    nh.getParam("camera", camera_id) ;

    if ( camera_id.empty() ) {
        ROS_ERROR("no camera given.") ;
        return 0 ;
    }


    GPhoto2Service srv ;

    if ( !srv.start(camera_id, nh) ) {
        ROS_FATAL( "gphoto2_service: Could not open camera %s.", camera_id.c_str() );
        nh.shutdown();
        return 0;
    }

    ros::spin() ;

}
