#include "camera.hpp"

#include <iostream>
#include <cvx/util/misc/path.hpp>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

using namespace std ;
using namespace cvx::util ;


namespace gphoto2 {

bool Camera::open( CameraList &list, const string &model_name, const string &port_name )
{
    // Create new camera
    if ( gp_camera_new( &handle_ ) != GP_OK ) return false ;

    // Find and set camera abilities based on model
    //std::cout << "Model name: " << model_name << " == " << std::endl;
    if ( list.lookupAbilities( model_name, &abilities_ ) ) {
        // Set the camera's abilities
        if( gp_camera_set_abilities( handle_, abilities_ ) != GP_OK ) return false ;
    }
    else return false ;

    // Associate camera with port
    if ( list.lookupPortInfo( port_name, &port_info_ ) )  {
        if( gp_camera_set_port_info( handle_, port_info_ ) != GP_OK ) return false ;
    }
    else return false ;

    if ( gp_camera_init(handle_, context_.handle()) != GP_OK ) return false ;

    name_ = model_name ;
    port_ = port_name ;

    // Camera is open!
    return true;
}

Camera::~Camera()
{
    close() ;
}

bool Camera::open(CameraList &clist, size_t cam_idx)
{
    const char *name, *value;

    if ( gp_list_get_name( clist.handle(), cam_idx, &name) != GP_OK ) return false ;
    if ( gp_list_get_value( clist.handle(), cam_idx, &value) != GP_OK ) return false ;

    return open(clist, name, value) ;
}

bool Camera::open(CameraList &clist, const std::string &port_id)
{
    size_t count = gp_list_count(clist.handle()) ;
    for ( size_t i=0 ; i<count ; i++ ) {
        const char *name, *value;
        gp_list_get_name( clist.handle(), i, &name);
        gp_list_get_value( clist.handle(), i, &value);
        if ( port_id.compare(value) == 0 ) {
            return open(clist, name, value) ;
        }
    }
    return false ;
}

bool Camera::close( ) {
    return ( gp_camera_exit( handle_, context_.handle() ) == GP_OK )  ;
}


bool Camera::capture( cv::Mat &photo )
{

    int error_code;
    CameraFile *photo_file;
    CameraFilePath photo_file_path;

    strcpy( photo_file_path.folder, "/" );
    strcpy( photo_file_path.name, "foo.jpg" );

    error_code = gp_camera_capture( handle_, GP_CAPTURE_IMAGE, &photo_file_path, context_.handle() );

    if( error_code < GP_OK ) return false ;

    // create temporary file
    string tmp_path = Path::tempFilePath("photo", ".jpg").toString() ;

    int fd = ::open(tmp_path.c_str(), O_WRONLY | O_CREAT | O_TRUNC,
                    S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH) ;

    if ( fd == -1 ) return false ;

    error_code = gp_file_new_from_fd( &photo_file, fd );

    if( error_code < GP_OK )  {
        ::close( fd );
        Path::remove(tmp_path) ;

        gp_file_free( photo_file );
        return false;
    }

    // get image from camera and store in temporary file
    error_code = gp_camera_file_get( handle_, photo_file_path.folder, photo_file_path.name, GP_FILE_TYPE_NORMAL, photo_file, context_.handle() );
    if( error_code < GP_OK )
    {
        gp_file_unref( photo_file );
        Path::remove(tmp_path) ;

        return false;
    }

    // delete image from camera's memory
    error_code = gp_camera_file_delete( handle_, photo_file_path.folder, photo_file_path.name, context_.handle() );
    if( error_code < GP_OK )
    {
        Path::remove(tmp_path) ;

        gp_file_free( photo_file );
        return false;
    }

    photo = cv::imread(tmp_path, -1) ;

    gp_file_free( photo_file );
    Path::remove(tmp_path) ;

    return photo.data != 0 ;

}

bool Camera::photo_camera_find_widget_by_name( std::string name, CameraWidget **child, CameraWidget **root) {

    size_t found_index = name.find( '/' );
    if ( found_index == string::npos ) {
        // Find child of configuration by name
        if ( gp_widget_get_child_by_name( *root, name.c_str(), child ) == GP_OK ) return true ;

        // Find child of configuration  by label
        if( gp_widget_get_child_by_label( *root, name.c_str(), child ) == GP_OK ) return true ;

        return false ;
    }
    else {
        string section = name.substr(0, found_index) ;
        string suffix = name.substr(found_index+1) ;

        CameraWidget *schild ;
        if ( gp_widget_get_child_by_name( *root, section.c_str(), &schild ) == GP_OK ) {
            return photo_camera_find_widget_by_name( suffix, child, &schild) ;

        }
        else return false ;
    }


    return false;
}

bool icompare_pred(unsigned char a, unsigned char b)
{
    return std::tolower(a) == std::tolower(b);
}

bool icompare(std::string const& a, std::string const& b)
{
    if (a.length()==b.length()) {
        return std::equal(b.begin(), b.end(),
                          a.begin(), icompare_pred);
    }
    else {
        return false;
    }
}
static bool check_toggle_value( const std::string &value_in, bool &res )
{
    vector<string> toggle_positive{ "on", "yes", "true", "1"};
    vector<string> toggle_negative{ "off", "no", "false", "0"};

    if ( std::find_if(toggle_positive.begin(), toggle_positive.end(), [&](const string &v) { return icompare(value_in, v) ; }) !=
         toggle_positive.end() ) {
        res = true ;
        return true ;
    }

    if ( std::find_if(toggle_negative.begin(), toggle_negative.end(), [&](const string &v) { return icompare(value_in, v) ; }) !=
         toggle_negative.end() ) {
        res = false ;
        return true ;
    }

    return false ;

}

bool Camera::setConfig(const string &param, const string &value) {

    CameraWidget *root, *child;
    int error_code;
    const char *label;
    CameraWidgetType type;

    // Locate the widget that corresponds to this parameter
    if ( !photo_camera_find_widget_by_name( param, &child, &root )  ) return false ;

    // Get the widget label
    if ( gp_widget_get_label(child, &label) != GP_OK ) {
        gp_widget_free( root );
        return false;
    }

    // Get the widget type
    if( gp_widget_get_type( child, &type ) != GP_OK )
    {
        gp_widget_free( root );
        return false;
    }

    switch( type )
    {

    case GP_WIDGET_TEXT: // char*
        if( gp_widget_set_value(child, value.c_str()) != GP_OK )
        {
            gp_widget_free( root );
            return false;
        }
        break;

    case GP_WIDGET_RANGE: // float
        float f, t, b, s;

        f = stof(value) ;

        if( gp_widget_get_range( child, &b, &t, &s) != GP_OK ||
                (f < b) || (f > t) ||
                gp_widget_set_value( child, &f ) != GP_OK )

        {
            gp_widget_free( root );
            return false;
        }

        break;

    case GP_WIDGET_TOGGLE: // int
        bool tog;
        if( check_toggle_value( value, tog ) == false ||
                gp_widget_set_value( child, &tog ) != GP_OK )
        {
            gp_widget_free( root );
            return false;
        }
        break;

    case GP_WIDGET_DATE: // int
    {
        int time = -1;

        if( !sscanf( value.c_str(), "%d", &time ) || gp_widget_set_value(child, &time) != GP_OK )  {
            gp_widget_free( root );
            return false;
        }
        break;
    }

    case GP_WIDGET_MENU:
    case GP_WIDGET_RADIO: // char*
        int count, i;
        count = gp_widget_count_choices( child );
        if( count < GP_OK )   {
            gp_widget_free( root );
            return false;
        }

        error_code = GP_ERROR_BAD_PARAMETERS;

        for( i = 0; i < count; i++ )
        {
            const char *choice;
            if ( gp_widget_get_choice( child, i, &choice ) == GP_OK )
            {
                if( value.compare( choice ) == 0 )
                {
                    if( gp_widget_set_value( child, value.c_str() ) == GP_OK )
                        break;
                }
            }
        }
        // attemt a different method for setting a radio button
        if( sscanf( value.c_str(), "%d", &i ) )
        {
            if( (i >= 0) && (i < count) )
            {
                const char *choice;
                if( gp_widget_get_choice( child, i, &choice ) == GP_OK )
                {
                    if( gp_widget_set_value( child, choice ) == GP_OK )
                        break;

                }
            }
        }
        gp_widget_free( root );
        return false;

    case GP_WIDGET_WINDOW:
    case GP_WIDGET_SECTION:
    case GP_WIDGET_BUTTON:
    default:
        gp_widget_free( root );
        return false;
    }


    // Configuration parameters are correct, so set the camera
    if( gp_camera_set_config( handle_, root, context_.handle() ) != GP_OK )
    {
        gp_widget_free( root );
        return false;
    }

    gp_widget_free( root );
    return true;
}



bool Camera::getConfig( const std::string &param, string &value )
{
    CameraWidget *root, *child;
    const char *label;
    CameraWidgetType type;

    // Get camera configuration
    if ( gp_camera_get_config( handle_, &root, context_.handle() ) != GP_OK ) return false ;

    // Locate the widget that corresponds to this parameter
    if ( !photo_camera_find_widget_by_name( param, &child, &root ) ) return false ;


    // Get the widget label
    if ( gp_widget_get_label(child, &label) != GP_OK )
    {
        gp_widget_free( root );
        return false;
    }

    // Get the widget type
    if ( gp_widget_get_type( child, &type ) != GP_OK )
    {
        gp_widget_free( root );
        return false;
    }

    switch( type )
    {
    case GP_WIDGET_TEXT: // char*
        char *txt;
        if( gp_widget_get_value( child, &txt ) == GP_OK )
            value = txt;
        break;

    case GP_WIDGET_RANGE: // float
        float f, t,b,s;
        if ( gp_widget_get_range( child, &b, &t, &s ) == GP_OK &&
             gp_widget_get_value( child, &f ) == GP_OK )
            value = to_string(f) ;

        break;

    case GP_WIDGET_TOGGLE: // int
    {
        int t;
        if( gp_widget_get_value( child, &t ) == GP_OK )
            value = to_string(t) ;
        break;
    }

    case GP_WIDGET_DATE: // int
    {
        int error_code, t;
        time_t working_time;
        struct tm *localtm;
        char timebuf[200];

        if( gp_widget_get_value( child, &t ) == GP_OK )
        {
            working_time = t;
            localtm = localtime( &working_time );
            error_code = strftime( timebuf, sizeof(timebuf), "%c", localtm );
            value = timebuf ;
        }
        break;
    }

    case GP_WIDGET_MENU:
    case GP_WIDGET_RADIO: //char*
        char *current;
        if( gp_widget_get_value (child, &current) == GP_OK )
            value = current ;
        break;

        // No values, so nothing to return
    case GP_WIDGET_WINDOW:
    case GP_WIDGET_SECTION:
    case GP_WIDGET_BUTTON:
    default:
        break;
    }

    gp_widget_free( root );
    return true;
}

}
