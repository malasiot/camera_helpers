#ifndef __GPHOTO2_CONTEXT_HPP__
#define __GPHOTO2_CONTEXT_HPP__

#include <gphoto2/gphoto2-context.h>

namespace gphoto2 {

class Context {
public:
    Context() ;

    GPContext *handle() { return handle_ ; }

    ~Context() {
        gp_context_unref(handle_);
    }

private:
    GPContext *handle_ ;
} ;



}


#endif
