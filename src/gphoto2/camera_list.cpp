#include "camera_list.hpp"
#include "context.hpp"

#include <iostream>

using namespace std ;

namespace gphoto2 {

CameraList::~CameraList() {
    gp_list_unref( camera_list_ );
    gp_port_info_list_free( port_info_list_ );
    gp_abilities_list_free( abilities_list_ );
}

bool CameraList::autoDetect() {

    Context context ;

    ssize_t port_count = 0;

    // Create a new list of cameras
    if( gp_list_new( &camera_list_ ) != GP_OK )
        return false ;

    // Load the low-level port drivers

    if( !loadPortInfo( &port_count ) ) return false ;

    // Load the photo_camera drivers

    if( !loadAbilities( context.handle() ) ) return false;

    // Filter the list for USB cameras
    if( !filterCameraList( context.handle(), "usb:" ) ) return false ;

    return true;
}

struct AutoCameraList {

    bool create() {
        return gp_list_new( &handle_ ) == GP_OK ;
    }

    ~AutoCameraList() {
        gp_list_free(handle_) ;
    }

    GPCameraList *handle_ = nullptr;
};

bool CameraList::filterCameraList( GPContext* context, const std::string match_string ) {

   AutoCameraList working_list ;

   if ( !working_list.create() ) return false ;

    // Autodetect the currently attached photo_cameras.
    if ( gp_abilities_list_detect( abilities_list_, port_info_list_, working_list.handle_, context) != GP_OK ) return false ;

    int count = gp_list_count( working_list.handle_ );

    if ( count < GP_OK ) return false ;

    // Clear camera_list_ for appending
    if ( gp_list_reset( camera_list_ ) != GP_OK ) return false ;

    // Filter out the generic 'usb:' entry
    for( int i = 0; i < count; i++ ) {
        const char *name, *value;
        gp_list_get_name( working_list.handle_, i, &name );
        gp_list_get_value( working_list.handle_, i, &value );

        if ( match_string.compare( value ) != 0 ) {
            gp_list_append( camera_list_, name, value );
        }
    }

    return true;
}



bool CameraList::loadPortInfo( ssize_t* port_count )
{
    if ( port_info_list_ == NULL ) {
        // Create a new port info list
        if ( gp_port_info_list_new( &port_info_list_ ) != GP_OK ) return false ;
    }

    // Populate the list
    if ( gp_port_info_list_load( port_info_list_ ) != GP_OK ) return false ;

    // Count the number of ports in the list
    *port_count =  gp_port_info_list_count( port_info_list_ );

    if( *port_count < GP_OK )
        return false ;

    return true;
}

bool CameraList::loadAbilities( GPContext* context )
{
    // Create a new abilities list
    if ( gp_abilities_list_new( &abilities_list_ ) != GP_OK ) return false ;

    // Populate the abilities list
    if( gp_abilities_list_load( abilities_list_, context ) != GP_OK ) return false ;

    return true;
}

bool CameraList::lookupPortInfo( const std::string port_name, GPPortInfo* port_info ) {
    int list_index = 0;

    // Find the port in the list of ports and return the index
    list_index = gp_port_info_list_lookup_path( port_info_list_, port_name.c_str() );

    if( list_index < GP_OK ) return false ;

    // Get the port information from from the information list
    if ( gp_port_info_list_get_info( port_info_list_, list_index, port_info ) != GP_OK )
        return false ;

    return true;
}

bool CameraList::lookupAbilities( const std::string model_name, CameraAbilities* abilities )
{
    int list_index = 0;

    // Find the camera in the list of cameras and return the index
    list_index = gp_abilities_list_lookup_model( abilities_list_, model_name.c_str() );
    if( list_index < GP_OK ) return false ;

    // Find the camera's abilities in the abilities list
    if( gp_abilities_list_get_abilities( abilities_list_, list_index, abilities ) != GP_OK ) return false ;

    return true;
}

}
