#include "context.hpp"

#include <iostream>

using namespace std ;

void gp_error_func (GPContext *context, const char *text, void *data) {
    cerr << text << endl ;
}

void gp_message_func (GPContext *context, const char *text, void *data) {
    cout << text << endl ;
}

namespace gphoto2 {

Context::Context() {
    handle_ = gp_context_new() ;

    gp_context_set_error_func( handle_, gp_error_func, NULL );
    gp_context_set_status_func( handle_, gp_message_func, NULL );
}

}
