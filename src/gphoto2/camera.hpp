#ifndef __GPHOTO2_CAMERA_HPP__
#define __GPHOTO2_CAMERA_HPP__

#include <opencv2/opencv.hpp>

#include <gphoto2/gphoto2-camera.h>
#include <gphoto2/gphoto2-context.h>
#include <gphoto2/gphoto2-setting.h>
#include <gphoto2/gphoto2-filesys.h>

#include <string>
#include <mutex>
#include <memory>

#include "context.hpp"
#include "camera_list.hpp"

typedef Camera GPCamera ;

namespace gphoto2 {

class Camera {
public:

    Camera() {}
    ~Camera() ;

    bool open(CameraList &clist, size_t cam_idx) ;
    bool open(CameraList &clist, const std::string &port_name);
    bool open(CameraList &clist, const std::string &model_name, const std::string &port_name);
    bool close();

    std::string name() const { return name_ ; }
    std::string port() const { return port_ ; }


    bool capture(cv::Mat &im);

    bool getConfig(const std::string &param, std::string &value);
    bool setConfig(const std::string &param, const std::string &value);


private:
    GPCamera *handle_ = nullptr ;
    Context context_ ;
    GPPortInfo port_info_;
    CameraAbilities abilities_;
    std::string name_, port_ ;

    bool photo_camera_find_widget_by_name(std::string name, CameraWidget **child, CameraWidget **root);
};

}
#endif
