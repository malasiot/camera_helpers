#ifndef __GPHOTO2_CAMERA_LIST_HPP__
#define __GPHOTO2_CAMERA_LIST_HPP__

#include <gphoto2/gphoto2-camera.h>
#include <gphoto2/gphoto2-context.h>
#include <gphoto2/gphoto2-setting.h>
#include <gphoto2/gphoto2-filesys.h>

#include <string>
#include <mutex>

typedef CameraList GPCameraList ;

namespace gphoto2 {

class CameraList
{
public:
    CameraList() = default ;
    ~CameraList();

    GPCameraList* handle() const { return camera_list_ ; }
    GPPortInfoList* getPortInfoList( void );
    CameraAbilitiesList* getAbilitiesList( void );

    //* Autodetect all photo_cameras connected to the system
    bool autoDetect( );

    //* Look up the port information for the port 'port_name'
    bool lookupPortInfo( const std::string port_name, GPPortInfo* port_info );
    //* Look up abilities for the camera model 'model_name'
    bool lookupAbilities( const std::string model_name, CameraAbilities* abilities );

private:

    bool loadPortInfo( ssize_t* port_count );
    bool loadAbilities( GPContext* context );
    bool filterCameraList( GPContext* context, const std::string match_string );

private:
    GPCameraList* camera_list_ = nullptr ;
    GPPortInfoList* port_info_list_ = nullptr ;
    CameraAbilitiesList* abilities_list_ = nullptr ;
};


}
#endif
