#include <camera_helpers/openni_capture.hpp>
#include <camera_helpers/util.hpp>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/PointCloud2.h>

#include <ros/callback_queue.h>
//#include <boost/scoped_ptr.hpp>
//#include <boost/thread.hpp>
#include <thread>
#include <mutex>
#include <functional>
#include <condition_variable>
#include <atomic>

namespace enc = sensor_msgs::image_encodings;

using namespace std ;
using namespace sensor_msgs;
using namespace message_filters;

namespace camera_helpers {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class OpenNICaptureImpl {
public:
    OpenNICaptureImpl(const std::string &prefix): prefix_(prefix),
        connected_(false), data_ready_(false),nh_("~") {
        nh_.setCallbackQueue( &callback_queue_ );
    }

    bool connect(ros::Duration timeout) ;

    void connect(std::function<void ()> cb, ros::Duration timeout) ;

    virtual void disconnect() ;

    bool isConnected() const {
        std::unique_lock<std::mutex> lock (image_lock_) ;
        return connected_ ;
    }

protected:
    virtual void setup() = 0;
    virtual void shutdown() = 0;

    void waitForConnection(ros::Duration t) ;
    void connectCb(boost::function<void ()> cb, ros::Duration t) ;
    void spinThread() ;
    bool hasTopic(const string &topicName) ;

    std::string prefix_ ;

    ros::NodeHandle nh_;

    mutable std::mutex image_lock_ ;
    mutable std::mutex data_ready_mutex_ ;
    std::condition_variable connected_cont_ ;

    bool connected_, data_ready_ ;
    ros::CallbackQueue callback_queue_;

    std::unique_ptr<std::thread> spin_thread_;
    std::atomic<bool> need_to_terminate_{false} ;

};


bool OpenNICaptureImpl::hasTopic(const string &topicName)
{
    ros::master::V_TopicInfo topic_info;
    ros::master::getTopics(topic_info);

    for (ros::master::V_TopicInfo::const_iterator it = topic_info.begin(); it != topic_info.end(); it++)
    {
        if ( it->name == topicName ) return true ;
    }

    return false ;

}

void OpenNICaptureImpl::spinThread()
{
    while (nh_.ok())
    {
        if ( need_to_terminate_ ) break ;

        callback_queue_.callAvailable(ros::WallDuration(0.033f));
    }
}



bool OpenNICaptureImpl::connect(ros::Duration timeout)
{
    if ( connected_ ) disconnect() ;

    setup() ;

    spin_thread_.reset( new std::thread(&OpenNICaptureImpl::spinThread, this) );

    if ( timeout.toSec() == -1 )
    {
        std::unique_lock<std::mutex> lock(data_ready_mutex_) ;
        while ( !data_ready_ )
            connected_cont_.wait(lock);

        connected_ = true ;
    }
    else  {
        std::unique_lock<std::mutex> lock(data_ready_mutex_) ;

        connected_ = connected_cont_.wait_for(lock, std::chrono::nanoseconds(timeout.toNSec()), [&]() { return data_ready_ ;});
    }

    if ( !connected_ )
        disconnect() ;

    return connected_ ;
}

void OpenNICaptureImpl::connectCb(boost::function<void ()> cb, ros::Duration timeout)
{
    if ( timeout.toSec() == -1 )
    {
        std::unique_lock<std::mutex> lock(data_ready_mutex_) ;
        while ( !data_ready_ )
            connected_cont_.wait(lock);

        connected_ = true ;
    }
    else  {
        std::unique_lock<std::mutex> lock(data_ready_mutex_) ;
        connected_ = connected_cont_.wait_for(lock, std::chrono::nanoseconds(timeout.toNSec()), [&]() { return data_ready_ ;});
    }

    if ( !connected_ ) {
        disconnect() ;
        return ;
    }


    cb() ;

}

void OpenNICaptureImpl::connect(std::function<void ()> cb, ros::Duration timeout)
{
    {
        std::unique_lock<std::mutex> lock (image_lock_) ;

        if ( connected_ ) disconnect() ;
    }

    setup() ;

    spin_thread_.reset( new std::thread(&OpenNICaptureImpl::spinThread, this) );

    // start connection thread

    std::thread t(std::bind(&OpenNICaptureImpl::connectCb, this, cb, timeout)) ;
}



void OpenNICaptureImpl::disconnect()
{
    std::unique_lock<std::mutex> lock (image_lock_) ;

    shutdown() ;

    ros::Duration(0.5).sleep() ;

    if ( spin_thread_.get() )
    {
        need_to_terminate_ = true;
        spin_thread_->join();

    }

    connected_ = false ;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class OpenNICaptureImplRGBD: public OpenNICaptureImpl
{
public:

    OpenNICaptureImplRGBD(const string &prefix_): OpenNICaptureImpl(prefix_),
        sync_(sync_policies::ApproximateTime<Image, Image, CameraInfo>(25), rgb_sub_, depth_sub_, camera_sub_)
    {
        sync_.registerCallback(boost::bind(&OpenNICaptureImplRGBD::input_callback, this, _1, _2, _3));
     }


    bool grab(cv::Mat &out_clr, cv::Mat &out_depth, ros::Time &t, image_geometry::PinholeCameraModel &cm, bool scale_depth)
    {
        if ( !connected_ ) return false ;

        std::unique_lock<std::mutex> lock (image_lock_) ;

        if ( !tmp_rgb_ || !tmp_depth_ ) return false ;

        cv::Mat clr = camera_helpers::msgToCvImage(tmp_rgb_) ;

        cv::Mat depth_unscaled = camera_helpers::msgToCvImage(tmp_depth_) ;
        cv::Mat depth ;

        if ( scale_depth ) depth_unscaled.convertTo(depth, CV_16UC1, 1000.0) ;

        out_clr = clr ;
        out_depth = (scale_depth ) ? depth : depth_unscaled ;
        cm.fromCameraInfo(tmp_camera_) ;

        t = tmp_depth_->header.stamp ;

        if ( out_clr.data == NULL || out_depth.data == NULL ) return false ;
        else return true ;
    }


private:

    void setup()
    {
        // Subscribe to rgb and depth streams

        rgb_sub_.subscribe(nh_, "/" + prefix_ + "/rgb/image_rect_color", 10);

        string depthTopic ;

        if ( hasTopic(depthTopic = "/" + prefix_ + "/depth_registered/sw_registered/image_rect_raw") )
            depth_sub_.subscribe(nh_, depthTopic, 10);
        else if (  hasTopic(depthTopic = "/" + prefix_ + "/depth_registered/hw_registered/image_rect_raw") )
            depth_sub_.subscribe(nh_, depthTopic, 10);

        camera_sub_.subscribe(nh_, "/" + prefix_ + "/depth_registered/camera_info", 10);
    }

    virtual void shutdown()
    {
        rgb_sub_.unsubscribe();
        depth_sub_.unsubscribe();
        camera_sub_.unsubscribe() ;
    }


    sensor_msgs::ImageConstPtr tmp_rgb_, tmp_depth_ ;
    sensor_msgs::CameraInfoConstPtr tmp_camera_ ;
    message_filters::Subscriber<sensor_msgs::Image> rgb_sub_, depth_sub_ ;
    message_filters::Subscriber<sensor_msgs::CameraInfo> camera_sub_ ;
    message_filters::Synchronizer<message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::Image, sensor_msgs::CameraInfo> > sync_ ;

    void input_callback(const ImageConstPtr& rgb, const ImageConstPtr& depth, const CameraInfoConstPtr &camera)
    {
        {
           std::unique_lock<std::mutex> lock (image_lock_) ;

            // Store current images
            tmp_rgb_ = rgb ;
            tmp_depth_ = depth ;
            tmp_camera_ = camera ;
        }


        if ( !connected_ )  { // notify thread that data has been received
            {
                std::unique_lock< std::mutex > lock( data_ready_mutex_ );
                data_ready_ = true;
            }
            connected_cont_.notify_one();
        }
    }
};


bool OpenNICaptureBase::connect(ros::Duration timeout) {
    return impl_->connect(timeout) ;
}


void  OpenNICaptureBase::connect(boost::function<void ()> cb, ros::Duration timeout) {
    impl_->connect(cb, timeout) ;
}

// disconnect from the camera
void OpenNICaptureBase::disconnect() {
    impl_->disconnect();
}

bool OpenNICaptureBase::isConnected() const {
    return impl_->isConnected() ;
}

OpenNICaptureBase::~OpenNICaptureBase() {

}


OpenNICaptureRGBD::OpenNICaptureRGBD(const string &prefix_) {
    impl_.reset(new OpenNICaptureImplRGBD(prefix_)) ;
}

bool OpenNICaptureRGBD::grab(cv::Mat &clr, cv::Mat &depth, ros::Time &t, image_geometry::PinholeCameraModel &cm, bool scale_depth)
{
    boost::shared_ptr<OpenNICaptureImplRGBD> ip = boost::dynamic_pointer_cast<OpenNICaptureImplRGBD>(impl_) ;
    return ip->grab(clr, depth, t, cm, scale_depth) ;
}



}
