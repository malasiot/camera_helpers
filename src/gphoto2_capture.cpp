#include <camera_helpers/gphoto2_capture.hpp>

#include <ros/node_handle.h>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>

#include <sensor_msgs/image_encodings.h>

namespace enc = sensor_msgs::image_encodings;

#include <camera_helpers/gphoto2_capture.h>
#include <camera_helpers/gphoto2_get_config.h>
#include <camera_helpers/gphoto2_set_config.h>

namespace camera_helpers {
namespace gphoto2 {

bool capture(cv::Mat &im, const std::string srv_prefix)
{
    ros::NodeHandle n;
    ros::ServiceClient client = n.serviceClient<camera_helpers::gphoto2_capture>(srv_prefix + "/gphoto2_service/capture");
    camera_helpers::gphoto2_capture srv;

    if ( client.call(srv) ) {
        cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(srv.response.image, enc::BGR8);
        im = cv_ptr->image ;
        return true ;
    }
    else
        return false ;
}

bool setConfig(const std::string &name, const std::string &val, const std::string srv_prefix)
{
    ros::NodeHandle n(srv_prefix);
    ros::ServiceClient client = n.serviceClient<camera_helpers::gphoto2_set_config>(srv_prefix + "/gphoto2_service/set_config");
    camera_helpers::gphoto2_set_config srv;
    srv.request.param = name ;
    srv.request.value = val ;

    return client.call(srv) ;
}

bool getConfig(const std::string &name, std::string &val, const std::string srv_prefix)
{
    ros::NodeHandle n;
    ros::ServiceClient client = n.serviceClient<camera_helpers::gphoto2_get_config>(srv_prefix + "/gphoto2_service/get_config");
    camera_helpers::gphoto2_get_config srv;

    srv.request.param = name ;

    if ( client.call(srv) ) {
        val = srv.response.value ;
        return true ;
    }
    else return false ;
}

}

}
