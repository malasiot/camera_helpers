#!/bin/bash 

IFS=$'\n'

if [ "$2" == "" ]; then
	CMD=`lsusb -v 2>/dev/null`
else
	CMD=`ssh "$2" lsusb -v 2>/dev/null`
fi

rx="Bus ([0-9]+)\ Device\ ([0-9]+).*"
rx_serial=".*iSerial[ \t]+[0-9]+[ \t]*"
serial_in=$1
serial=${serial_in:1}
rx_serial="$rx_serial $serial_in"

for line in $CMD
do
		
  if [[ "$line" =~ $rx ]]; then
	
	STR=`echo $line | sed -r 's/Bus ([0-9]+) Device ([0-9]+).*/usb:\1,\2/'`
	BUS_DEV=$STR
		
  elif [[ "$line" =~ $rx_serial ]];then
	echo -n $BUS_DEV
  fi
done
